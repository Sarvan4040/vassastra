
<?php
/**
 * @package WordPress
 * @subpackage workz Theme
 */
 $options = get_option( 'workz_theme_settings' );
?>
<div id="sidebar" class="clearfix">
	<?php dynamic_sidebar('sidebar'); ?>
</div>
<!-- END sidebar -->

<!-- <div class="widget_addons"><a href="http://www.books2help.com/books/computers-internet/software-engineering">software engineering books</a></div> -->