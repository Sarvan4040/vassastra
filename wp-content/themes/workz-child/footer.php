<?php
/**
 * @package WordPress
 * @subpackage workz Theme
 */
$options = get_option( 'workz_theme_settings' );
?>
<div class="clear"></div>

</div>
<!-- END wrap --> 

    <div id="footer-wrap">
    <div id="footer">
    
        <div id="footer-widget-wrap" class="clearfix">
    
            <div id="footer-left">
            <?php dynamic_sidebar('footer-left'); ?>
            </div>
            
            <div id="footer-middle">
             <?php dynamic_sidebar('footer-middle'); ?>
            </div>
            
            <div id="footer-right">
             <?php dynamic_sidebar('footer-right'); ?>
            </div>
        
        </div>
    
        <div id="footer-bottom">
        
            <div id="copyright">
                &copy; 2018 Frisör och skönhetssalong i Malmö &#8211; Vassa strå<br/>Kattsundsg. 18, 211 26 Malmö
            </div>
            
            <div id="back-to-top">
                <a href="http://www.dandypark.se" target="_blank">Producerad av Dandypark</a>
            </div>
        
        </div>
    
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
<script type="text/javascript">
jQuery(document).ready(function(){

    jQuery(window).bind("load resize", function(){  
        setTimeout(function() {
            var container_width = jQuery(window).width();
            if (container_width < 768) {
                var iframe_width = jQuery(".fb-iframe").width();
                jQuery('.fb-iframe').html('<iframe src="//www.facebook.com/plugins/likebox.php?locale=sv_SE&href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FVassa-str%25C3%25A5%2F132403796815123%3Fref%3Dts&amp;width='+ iframe_width + '&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;border_color=%23dddddd&amp;stream=false&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:'+ iframe_width+'px; height:258px;" allowTransparency="true"></iframe>');
            }
        }, 50);
    });

    jQuery(".sf-menu").append("<span class='close-image'></span>");

    jQuery(".close-image").on("click", function(){
        jQuery(".sf-menu").hide();
        return false;
    });

    jQuery(".menu-main-menu-container").on("click", function(){
        jQuery(".sf-menu").toggle();
        jQuery(".close-image").show();
    });

});
</script>