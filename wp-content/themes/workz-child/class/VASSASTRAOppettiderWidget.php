<?php
class VASSASTRAOppettiderWidget extends WP_Widget {

	function __construct() {
        parent::__construct(
            'vassastra_oppettider_widget', // Base ID
            esc_html__( 'VASSASTRA:Oppettider Box', '' ), // Name
            array( 'description' => esc_html__( 'Oppettider widget', '' ) ) // Args
        );
    }

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		
		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;

		?>
		
			<table class="op-overview-table"><tbody><tr class="op-overview-row "><th class="op-overview-title">Måndag</th><td class="op-overview-times"><div class="op-overview-set ">10:00 – 19:00</div></td></tr><tr class="op-overview-row "><th class="op-overview-title">Tisdag</th><td class="op-overview-times"><div class="op-overview-set ">10:00 – 19:00</div></td></tr><tr class="op-overview-row "><th class="op-overview-title">Onsdag</th><td class="op-overview-times"><div class="op-overview-set ">10:00 – 19:00</div></td></tr><tr class="op-overview-row "><th class="op-overview-title">Torsdag</th><td class="op-overview-times"><div class="op-overview-set ">10:00 – 19:00</div></td></tr><tr class="op-overview-row "><th class="op-overview-title">Fredag</th><td class="op-overview-times"><div class="op-overview-set ">10:00 – 19:00</div></td></tr><tr class="op-overview-row "><th class="op-overview-title">Lördag</th><td class="op-overview-times"><div class="op-overview-set ">10:00 – 17:00</div></td></tr><tr class="op-overview-row "><th class="op-overview-title">Söndag</th><td class="op-overview-times"><div class="op-overview-set ">11:00 – 17:00</div></td></tr></tbody></table>
			
			
		<?php

		/* After widget (defined by themes). */
		echo $after_widget;
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );

		return $instance;
	}


	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Hours');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:96%;" />
		</p>
	<?php
	}
}
?>