<?php /* Template Name: Homepage */ 
get_header();
?>
	<div class="home-wrap clearfix">
		<h1 id="home-tagline">Frisör och skönhetssalong i Malmö</h1>
		<div id="slider-wrap">
			<div id="slider_nivo" class="nivoSlider">
				<img src="http://vassastra.se/wp-content/uploads/2016/01/vassastra_0102.jpg" alt="Vassa strå &#8211; Frisör i Malmö" title="Vassa strå &#8211; Frisör i Malmö" width="920" height="300" />
				<img src="http://vassastra.se/wp-content/uploads/2012/06/vassastra-slide-02.jpg" alt="Frisörsalongen i Malmö, Kv. Caroli" title="Frisörsalongen i Malmö, Kv. Caroli" width="920" height="300" />
				<img src="http://vassastra.se/wp-content/uploads/2012/07/vassastra-slide-01.jpg" alt="Vi arbetar med Matrix, Alfaparf och Suki Skincare." title="Vi arbetar med Matrix, Alfaparf och Suki Skincare." width="920" height="300" />
			</div>
		</div>
		<div id="home-highlights" class="clearfix">
			<div class="hp-highlight ">
				<h2>Vassa strå – Frisör i Malmö</h2>
				<p>Are you looking for a good hairdresser in Malmö city? Then you are welcome to us.&nbsp;<strong>Vassa Strå</strong>&nbsp;is a hair salon and beauty salon that offers most hair, skin and massage treatments. You can find us in Kv. Caroli in the middle of Malmö, read more <a title="About us" href="http://vassastra.se/om-oss/">about Vassa Strå here.</a></p>
				<p>Welcome wish Emelie, Anna, Sneki, Adhraa, Ida and Louise.</p>
			</div>
			<div class="hp-highlight">
				<h2>Gilla oss på Facebook</h2>
				<p class="iframe"><iframe src="//www.facebook.com/plugins/likebox.php?locale=sv_SE&amp;href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FVassa-str%25C3%25A5%2F132403796815123%3Fref%3Dts&amp;width=290&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;border_color=%23dddddd&amp;stream=false&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:290px; height:258px;" allowtransparency="true"></iframe>
				</p>
			</div>
			<div class="hp-highlight highlight-last">
				<h2>Öppettider</h2>
				<table width="100%">
					<tbody>
						<tr>
							<td>Monday</td>
							<td>10:00 - 19:00</td>
						</tr>
						<tr>
							<td>Tuesday</td>
							<td>10:00 - 19:00</td>
						</tr>
						<tr>
							<td>Wednesday</td>
							<td>10:00 - 19:00</td>
						</tr>
						<tr>
							<td>Thursday</td>
							<td>10:00 - 19:00</td>
						</tr>
						<tr>
							<td>Friday</td>
							<td>10:00 - 19:00</td>
						</tr>
						<tr>
							<td>Saturday</td>
							<td>10:00 - 17:00</td>
						</tr>
						<tr>
							<td>Sunday</td>
							<td>11:00 - 17:00</td>
						</tr>
					</tbody>
				</table>
				<p>Tel.&nbsp;<strong>040 - 12 26 30</strong></p>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<?php get_footer(); ?>