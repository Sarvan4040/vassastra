��    2      �  C   <      H     I  
   U     `     v     �     �     �     �     �     �     
            4   &  %   [  
   �     �     �     �     �     �     �     �             	   ,     6     =     F     S  k   \  
   �     �  F   �  T   !  W   v     �     �     �     �     �                <     Q  	   g     q     }     �  �  �     r
     �
     �
  "   �
     �
     �
               0     N     n     }     �  N   �  -   �       	     #   %  (   I  	   r     |     �     �     �  !   �     �     �          	  
     �   #     �     �  h   �  R   6  b   �     �     �          
          '  %   G     m      �     �     �     �     �     )   	                       
   %   (      .       2         &      ,             /           0   *                   "   #       !      '                           1                        $                 +                    -       Add Holiday Add Period Also show closed days Caption for "closed"-label Choose a date format Choose a time format Closed Custom closed-holiday-message Custom closed-message Custom open-message Date Format End Date Friday Give your Holiday a name and set start and end date. Highlight currently running Holidays. Highlight: Holidays Insert your own date-format. Insert your own time-format. Monday Name Opening Hours Opening Hours Holidays Opening Hours Overview Opening Hours Status Read more Remove Saturday Save Changes Settings Setup your Opening Hours in this form. You can add several periods per day, e.g. if you have a lunch break. Start Date Sunday This Widget lists up all Holidays set up in the Opening Hours Section. This Widgets displays an overview of your Opening Hours in the corresponding Sidebar This Widgets displays information in your sidebar, wether your venue is closed or open. Thursday Time Format Title Tuesday We're currently closed We're currently closed. We're currently on holiday. We're currently open We're currently open. Wednesday current day nothing running period Project-Id-Version: Otevírací doba
POT-Creation-Date: 2013-11-03 15:09+0100
PO-Revision-Date: 2013-12-02 14:51+0100
Last-Translator: jholasek <jholasek@gmail.com>
Language-Team: Jannik Portz (CZ - jholasek) <webmaster@jannikportz.de>
Language: cs_CZ
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;op__;op_e
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
 Přidat dovolenou Přidat rozpětí Zobrazit i dny kdy je zavřeno Vlastní text pro 'Zavřeno' -Text Vyberte formát pro datum Vyberte formát času Zavřeno Vlastní zpráva pro Dovolená Vlastní zpráva pro Zavřeno  Vlastní zpráva pro Otevřeno Formát datumu Datum - Konec Pátek Napište název pro vaši dovolenou a nastavte počáteční a koncové datum. Zvýrazněte aktuálně běžící dovolené. Zvýraznit: Dovolená Vložte vlastní formát pro datum. Vložte svůj vlastní časový formát. Pondělí Název Otevírací doba Otevírací doba: Dovolená Otevírací doba - přehled Otevírací doba: aktuální stav Číst více Smazat Sobota Uložit změny Nastavení Nastavení vlastní Otevírací doby v této podobě. Můžete přidat několik rozpětí za den, například pokud máte přestávku na oběd. Datum - Začátek Neděle Tento widget se zobrazí seznam všech dovolených, které byly uvedeny v nastaveních Otevírací doby. Tento Widget zobrazí informace - přehled otevírací doby v postranním panelu.. Tento Widget zobrazí informace v postranním panelu, zda máte právě otevřeno a nebo zavřeno. Čtvrtek Formát času Název Úterý Nyní máme zavřeno Právě teď máme zavřeno :-( V současné době jsme na dovolené. Nyní máme otevřeno Právě teď máme otevřeno :-) Středa aktuální den nic aktuálně běží 