��    2      �  C   <      H     I  
   U     `     v     �     �     �     �     �     �     
            4   &  %   [  
   �     �     �     �     �     �     �     �             	   ,     6     =     F     S  k   \  
   �     �  F   �  T   !  W   v     �     �     �     �     �                <     Q  	   g     q     }     �  �  �     k
     }
     �
  %   �
     �
     �
     �
  -        6     Q     k  
   y     �  D   �  #   �  
   �        !   	     +     K     S     X     g     �     �  	   �  	   �     �     �     �  w   �     m     y  f   �  T   �  U   <  	   �     �     �     �     �     �     �          2     Q     Z     j     p     )   	                       
   %   (      .       2         &      ,             /           0   *                   "   #       !      '                           1                        $                 +                    -       Add Holiday Add Period Also show closed days Caption for "closed"-label Choose a date format Choose a time format Closed Custom closed-holiday-message Custom closed-message Custom open-message Date Format End Date Friday Give your Holiday a name and set start and end date. Highlight currently running Holidays. Highlight: Holidays Insert your own date-format. Insert your own time-format. Monday Name Opening Hours Opening Hours Holidays Opening Hours Overview Opening Hours Status Read more Remove Saturday Save Changes Settings Setup your Opening Hours in this form. You can add several periods per day, e.g. if you have a lunch break. Start Date Sunday This Widget lists up all Holidays set up in the Opening Hours Section. This Widgets displays an overview of your Opening Hours in the corresponding Sidebar This Widgets displays information in your sidebar, wether your venue is closed or open. Thursday Time Format Title Tuesday We're currently closed We're currently closed. We're currently on holiday. We're currently open We're currently open. Wednesday current day nothing running period Project-Id-Version: WP Opening Hours
POT-Creation-Date: 2013-11-03 15:09+0100
PO-Revision-Date: 2013-12-05 16:23+0100
Last-Translator: Jannik Portz <jannik.portz@conlabz.de>
Language-Team: Jannik Portz <webmaster@jannikportz.de>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;op__;op_e
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
 Voeg vakantie toe Periode toevoegen Ook gesloten dagen weergeven Gebruikers opgegeven "Gesloten"-Tekst Kies een Datum formaat Kies een tijd formaat Gesloten Aangepast gesloten-bericht (tijdens vakantie) Aangepast gesloten-bericht Aangepast geopend-bericht Datum formaat Eind-Datum Vrijdag Geef een naam in voor de vakantie en stel een start en eind tijd in. Highlight aktueel lopende vakantie. Highlight: Vakantie Voeg een eigen Datum formaat toe. Geef een eigen tijd-formaat in. Maandag Naam Openingstijden Openingstijden: Vakantie Openingstijden overzicht Openingstijden: Huidige Status Lees meer Verwijder Zaterdag Sla wijzigingen op Instellingen Stel uw Openingstijden in in dit formulier. U kunt meerdere periodes per dag toevoegen, indien u bv. middagpauze heeft. Start-Datum Zondag Deze Widget toont een lijst van alle vakanties, die in de Openingstijden-Instellingen vermeldt werden. Deze Widget toont een overzicht van de Openingstijden in de corresponderende Sidebar Deze Widget laat in de Sidebar zien, of u bedrijf momenteel geopend of gesloten bent. Donderdag Tijd formaat Titel Dinsdag We zijn op dit moment gesloten We zijn op dit moment gesloten. We hebben momenteel vakantie. We zijn op dit moment geopend We zijn op dit moment geopend. Woensdag Huidige weekdag niets Aktuele periode 