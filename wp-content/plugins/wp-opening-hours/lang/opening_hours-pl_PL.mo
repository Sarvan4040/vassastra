��    2      �  C   <      H     I  
   U     `     v     �     �     �     �     �     �     
            4   &  %   [  
   �     �     �     �     �     �     �     �             	   ,     6     =     F     S  k   \  
   �     �  F   �  T   !  W   v     �     �     �     �     �                <     Q  	   g     q     }     �  �  �     n
     �
  3   �
  !   �
     �
       
     0   $     U     s     �     �     �  D   �  0   �  
   *     5     G     e     �     �     �  #   �     �     �                    #  
   3  �   >     �  	   �  ;   �  '      _   H     �     �     �     �  
   �  
   �     �     �     �                         )   	                       
   %   (      .       2         &      ,             /           0   *                   "   #       !      '                           1                        $                 +                    -       Add Holiday Add Period Also show closed days Caption for "closed"-label Choose a date format Choose a time format Closed Custom closed-holiday-message Custom closed-message Custom open-message Date Format End Date Friday Give your Holiday a name and set start and end date. Highlight currently running Holidays. Highlight: Holidays Insert your own date-format. Insert your own time-format. Monday Name Opening Hours Opening Hours Holidays Opening Hours Overview Opening Hours Status Read more Remove Saturday Save Changes Settings Setup your Opening Hours in this form. You can add several periods per day, e.g. if you have a lunch break. Start Date Sunday This Widget lists up all Holidays set up in the Opening Hours Section. This Widgets displays an overview of your Opening Hours in the corresponding Sidebar This Widgets displays information in your sidebar, wether your venue is closed or open. Thursday Time Format Title Tuesday We're currently closed We're currently closed. We're currently on holiday. We're currently open We're currently open. Wednesday current day nothing running period Project-Id-Version: WP Opening Hours
POT-Creation-Date: 2013-11-03 15:09+0100
PO-Revision-Date: 2013-12-13 05:33+0100
Last-Translator: Bartosz Chojnacki <b.chojnacki@gmail.com>
Language-Team: Jannik Portz <webmaster@jannikportz.de>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;op__;op_e
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
 Dodaj święto / urlop Dodaj okres otwarcia Wyświetl również dni, w których jest zamknięte Etykieta dla statusu "Zamknięte" Wybierz format daty Wybierz format czasu Zamknięte Własna etykieta "zamknięte - urlop / święto" Własna etykieta "zamknięte" Własna etykieta "otwarte" Format daty Data zakończenia Piątek Dodaj nazwę świąt / urlopu oraz daty rozpoczęcia i zakończenia. Wyróżnij obecnie trwające święta  / urlopy. Wyróżnij Urlopy / Święta Wprowadź własny format daty Wprowadź własny format czasu Poniedziałek Nazwa Godziny otwarcia Godziny otwarcia: Urlopy / Święta Godziny otwarcia - przegląd Godziny otwarcia: Status Więcej informacji Usuń Sobota Zachowaj zmiany Ustawienia Tutaj możesz ustawić swoje godziny otwarcia. Dzień możesz podzielić na okresy, w których twój lokal jest czynny (np. 8-12 i 16-18) Data rozpoczęcia Niedziela Ten widget wyświetla święta zdefiniowane w ustawieniach. Ten widget wyświetla godziny otwarcia. Ten widget informuje odwiedzających, czy w danej chwili twój lokal jest zamknięty / otwarty. Czwartek Format czasu Tytuł Wtorek ZAMKNIĘTE ZAMKNIĘTE URLOP (zamknięte) OTWARTE OTWARTE Środa obecny dzień nic obecny okres 