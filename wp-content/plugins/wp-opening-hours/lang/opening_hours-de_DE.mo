��    2      �  C   <      H     I  
   U     `     v     �     �     �     �     �     �     
            4   &  %   [  
   �     �     �     �     �     �     �     �             	   ,     6     =     F     S  k   \  
   �     �  F   �  T   !  W   v     �     �     �     �     �                <     Q  	   g     q     }     �  �  �     k
     ~
     �
  &   �
     �
     �
     �
  /     "   ;      ^       	   �     �  C   �  $   �            "         >     _     f     k     {     �  !   �     �  	   �     �     �       �        �     �  i   �  r   $  X   �  
   �  
   �                    4     T     q     �     �     �     �     �     )   	                       
   %   (      .       2         &      ,             /           0   *                   "   #       !      '                           1                        $                 +                    -       Add Holiday Add Period Also show closed days Caption for "closed"-label Choose a date format Choose a time format Closed Custom closed-holiday-message Custom closed-message Custom open-message Date Format End Date Friday Give your Holiday a name and set start and end date. Highlight currently running Holidays. Highlight: Holidays Insert your own date-format. Insert your own time-format. Monday Name Opening Hours Opening Hours Holidays Opening Hours Overview Opening Hours Status Read more Remove Saturday Save Changes Settings Setup your Opening Hours in this form. You can add several periods per day, e.g. if you have a lunch break. Start Date Sunday This Widget lists up all Holidays set up in the Opening Hours Section. This Widgets displays an overview of your Opening Hours in the corresponding Sidebar This Widgets displays information in your sidebar, wether your venue is closed or open. Thursday Time Format Title Tuesday We're currently closed We're currently closed. We're currently on holiday. We're currently open We're currently open. Wednesday current day nothing running period Project-Id-Version: WP Opening Hours
POT-Creation-Date: 2013-11-03 15:09+0100
PO-Revision-Date: 2013-11-03 15:10+0100
Last-Translator: Jannik Portz <jannik.portz@conlabz.de>
Language-Team: Jannik Portz <webmaster@jannikportz.de>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;op__;op_e
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
 Ferien hinzufügen Zeitraum hinzufügen Auch Ruhetage anzeigen Benutzerdefinierter "Geschlossen"-Text Wähle ein Datumsformat Wähle ein Zeitformat Geschlossen Individuelle Geschlossen-Nachricht (bei Ferien) Individuelle Geschlossen-Nachricht Individuelle Geöffnet-Nachricht Datumsformat End-Datum Freitag Vergib einen Namen für deine Ferien und setze Start- und Enddatum. Aktuell laufende Ferien hervorheben. Hervorheben: Ferien Gib dein eigenes Datumsformat ein. Gib dein eigenes Zeitformat ein. Montag Name Öffnungszeiten Öffnungszeiten: Ferien Öffnungszeiten Übersicht Öffnungszeiten: Aktueller Status Erfahre mehr Entfernen Samstag Änderungen Speichern Einstellungen Stelle deine Öffnungszeiten in diesem Formular ein. Du kannst mehrere Perioden pro Tag hinzufügen, falls du zum Beispiel eine Mittagspause hast. Start-Datum Sonntag Dieses Widget zeigt eine Liste aller Ferien, die in den Öffnungszeiten-Einstellungen gesetzt wurden, an. Dieses Widget zeigt eine Übersicht Ihrer Öffnungszeiten an entsprechender Stelle in der gewünschten Sidebar an. Dieses Widget zeigt in Ihrer Sidebar an, ob Sie gerade geöffnet oder geschlossen haben. Donnerstag Zeitformat Titel Dienstag Wir haben zur Zeit geschlossen Wir haben zur Zeit geschlossen. Wir sind zur Zeit in Ferien. Wir haben zur Zeit geöffnet Wir haben zur Zeit geöffnet. Mittwoch aktuellen Wochentag nichts aktuelle Öffnung 