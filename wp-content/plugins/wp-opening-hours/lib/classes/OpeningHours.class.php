<?
/**
 *	Opening Hours Class
 */
 
class OpeningHours {
	
	function __construct() {
		$this->setup();
		$this->getSettings();
		$this->getHolidays();
	}
	
	/**
	 *	Setup – intializes opening hours
	 */
	function setup() {
		if (get_option('wp_opening_hours')) :
			$this->data		= json_decode( get_option('wp_opening_hours'), true );
		else :
			$this->data		= array();
		endif;
		
		$this->weekdays		= array(
			'monday'	=> op__('Måndag'), 
			'tuesday'	=> op__('Tisdag'),
			'wednesday'	=> op__('Onsdag'),
			'thursday'	=> op__('Torsdag'),
			'friday'	=> op__('Fredag'), 
			'saturday'	=> op__('Lördag'), 
			'sunday'	=> op__('Söndag')
		);
				
		foreach ( $this->data as $key => $values ) :
			$periods	= array();
			
			foreach ( $this->data[ $key ][ 'times' ] as $period ) :
				$periods[]	= new OpeningPeriod( array(
					'day'	=> $key,
					'times'	=> $period
				) );
			endforeach;
			
			$this->$key	= $periods;
		endforeach;		
	}
	
	/**
	 *	Save – Saves opening hours to wp_option
	 */
	function save() {
		$data		= array();
		foreach ($this->weekdays as $key => $caption) :
			if (is_array($this->$key))
				$periods	= $this->$key;
			
			foreach ($periods as $period) :
				
				if ($period->correctValues()) :
					$data[ $key ]['times'][]	= $period->times;
				else :
					unset( $period );
				endif;
				
			endforeach;
		endforeach;

		$success	= update_option( 'wp_opening_hours', json_encode( $data ) );
		$this->setup();
		return $success;
	}
			
	/**
	 *	Add Dummy Sets – adds an empty set for unset days
	 */
	function addDummySets() {
		foreach ($this->weekdays as $key => $caption) :
			if (!is_array( $this->$key ) or !count( $this->$key ))
				$this->$key		= array(
					new OpeningPeriod( array(
						'day'		=> $key,
						'times'		=> array(0, 0, 0, 0)
					) )
				);
		endforeach;
	}
	
	/**
	 *	Returns an array with all periods
	 */
	function allPeriods() {
		$periods		= array();
		
		foreach ($this->weekdays as $key => $caption) :
						
			if (is_array( $this->$key ))
				foreach ($this->$key as $period)
					$periods[]		= $period;
		endforeach;
		
		return $periods;
	}
	
	/**
	 *	Returns number of all periods
	 */
	 function numberPeriods() {
		return count( $this->allPeriods() ); 
	 }
	 
	 /**
	  *	Returns an array with all days
	  */
	 function allDays() {
		 $days		= array();
		 foreach ($this->weekdays as $key => $caption) :
		 	$days[ $key ]	= (is_array( $this->$key )) ? $this->$key : array();
		 endforeach;
		 return $days;
	 }
			
	/**
	 *	Settings
	 */
	function getSettings() {
		$this->settings		= json_decode( get_option('wp_opening_hours_settings'), true );
	}
	
	function saveSettings() {
		update_option( 'wp_opening_hours_settings', json_encode( $this->settings ) );
	}
	
	function applySettings( $args = array() ) {
		// Format of $args:		array( $setting_key => $value )
		foreach ( $args as $key => $value ) :
			$this->settings[ $key ]			= $value;
		endforeach;
		
		$this->saveSettings();
	}
	
	/**
	 *	Holidays
	 */
	function getHolidays() {
		$holidays		= json_decode( get_option('wp_opening_hours_holidays'), true );
		
		$this->holidays	= array();
		foreach ((array) $holidays as $holiday) :
		
			$this->holidays[]	= new HolidayPeriod( $holiday );
		
		endforeach;
	}
	
	function saveHolidays() {
		$holidays	= array();
		
		foreach ((array) $this->holidays as $holiday) :
			$holidays[]		= $holiday->data;
		endforeach;
		
		update_option('wp_opening_hours_holidays', json_encode( $holidays ));
	}
	
	function addHolidayDummy() {
		if (!count($this->holidays))
			$this->holidays		= array(
				new HolidayPeriod
			);
	}
	
	/**
	 *	HELPERS
	 */
	 
	function getSets( $key ) {
		return $this->data[ $key ][ 'times' ];
	}
	
	function numberSets( $key ) {
		return count($this->getSets( $key ));
	}
	
	function timeString( $key = false, $set = false, $edge = false ) {
		if (!$key or $set === false)		return;
		$string			= '';

		$setdata		= $this->data[ (string)$key ][ 'times' ][ (string)$set ];
		if ($edge == 'start') :
			return twoDigits($setdata[0]).':'.twoDigits($setdata[1]);
		elseif ($edge == 'end') :
			return twoDigits($setdata[2]).':'.twoDigits($setdata[3]);
		else :
			return twoDigits($setdata[0]).':'.twoDigits($setdata[1]).' – '.twoDigits($setdata[2]).':'.twoDigits($setdata[3]);
		endif;
	}
	
	function isRunning ( $key = false, $set = false ) {
		if (!$key or $set === false)		return;
		
		if ($key != strtolower( date('l', time()) ))	return false;

		$setdata		= $this->data[ (string)$key ][ 'times' ][ (string)$set ];
		$currentTime	= date('H', current_time('timestamp'))*100 + date('i', current_time('timestamp'));
		return	( $currentTime >= $setdata[0]*100 + $setdata[1] and $currentTime <= $setdata[2]*100 + $setdata[3] );
	}

// End of Class
}
?>