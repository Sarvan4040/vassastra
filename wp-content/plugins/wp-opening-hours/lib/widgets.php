<?
/**
 *	Opening Hours Widgets
 */

//	Register Widgets
function op_init_widgets() {
	register_widget ( 'Opening_Hours_Overview' 	);
	register_widget	( 'Opening_Hours_Status'	);
	register_widget	( 'Opening_Hours_Holidays' 	);
}
add_action ( 'widgets_init', 'op_init_widgets' );

//	Opening Hours Holidays Widget
class Opening_Hours_Holidays extends WP_Widget {
	
	function __construct() {
		$widget_opts = array(
			'classname'		=> 'widget_op_holidays',
			'description'	=> op__('This Widget lists up all Holidays set up in the Opening Hours Section.')
		);	
		
		$this->WP_Widget('widget_op_holidays', op__('Opening Hours Holidays'), $widget_opts);
		$this->alt_option_name	= 'widget_op_holidays';
	}
	
	function widget ($args, $instance) {
		extract ($args);
		global $wp_opening_hours;
		
		echo $before_widget;
		
		if ($instance['title']) :
			echo $before_title;
			echo apply_filters('op_holidays_widget_title', $instance['title']);
			echo $after_title;
		endif;
		
		// sort holidays
		$holidays		= array();
		foreach ($wp_opening_hours->holidays as $holiday) :
			$holidays[ $holiday->start_ts ]		= $holiday;
		endforeach;
		
		ksort( $holidays );
		
		if (count($holidays)) :
		?>
			<table class="op-holidays-table">
            <?	foreach ($holidays as $holiday) : ?>
            	<tr class="op-holiday <? if ($instance['highlighted'] and $holiday->isRunning()) echo 'highlighted' ?>">
                	<th>
                    	<? echo apply_filters( 'op_holidays_widget_name', $holiday->name ) ?>
                    </th>
                    <td>
                    	<? echo apply_filters( 'op_holidays_widget_date_string', dateString( array(
							'start'		=> $holiday->start_ts,
							'end'		=> $holiday->end_ts
						) ) )
						?>
                    </td>
                </tr>
            <?	endforeach; ?>
            </table>
        <?
        endif;
		
		echo $after_widget;
	}
	
	function update ($new_instance, $old_instance) {
		return $new_instance;
	}
	
	function form ($instance) {
	?>
		<p>
        	<label for="<? echo $this->get_field_id('title') ?>">
            	<? op_e('Title') ?>
            </label>
            <input type="text" name="<? echo $this->get_field_name('title') ?>" id="<? echo $this->get_field_id('title') ?>" value="<? echo $instance['title'] ?>" class="widefat" />
        </p>
        
        <p>
        	<label for="<? echo $this->get_field_id('highlighted') ?>">
            	<input type="checkbox" <? echo ($instance['highlighted']) ? 'checked="checked"' : '' ?> name="<? echo $this->get_field_name('highlighted') ?>" id="<? echo $this->get_field_id('highlighted') ?>" />
                <? op_e('Highlight currently running Holidays.') ?>
            </label>
        </p>
	<?
	}
	
}

//	Opening Hours Overview Widget
class Opening_Hours_Overview extends WP_Widget {
	
	function __construct() {
		$widget_ops = array(
			'classname' 	=> 'widget_op_overview', 
			'description' 	=> op__('This Widgets displays an overview of your Opening Hours in the corresponding Sidebar')
		);
	
		$this->WP_Widget('widget_op_overview', op__('Opening Hours Overview'), $widget_ops);
		$this->alt_option_name = 'widget_op_overview';
		
		$this->defaults = array(
			'title'				=> apply_filters( 'op_overview_widget_default_title', op__('Opening Hours') ),
			'caption-closed'	=> apply_filters( 'op_overview_widget_default_closed', op__('Closed') )
		);
	}
	
	function widget ($args, $instance) {
		extract ( $args );
		
		$instance	= self::setup_defaults( $instance );
		
		global $wp_opening_hours;
		if (!is_object($wp_opening_hours))	$wp_opening_hours	= new OpeningHours;
		
		echo $before_widget;
		
		echo $before_title;
		echo apply_filters( 'op_overview_widget_title', $instance['title'] );
		echo $after_title;
				
		if ($wp_opening_hours->numberPeriods() or $insance['show-closed']) :
		?>
			<table class="op-overview-table">
            <? 
				foreach ($wp_opening_hours->allDays() as $key => $periods) : 
				if (count( $periods ) or $instance['show-closed']) :
			?>
            	<tr class="op-overview-row <? echo ($instance['highlight'] == 'day' and $key == strtolower(date('l', current_time('timestamp')))) ? 'highlighted' : '' ?>">
                	<th class="op-overview-title">
                    	<? echo apply_filters( 'op_overview_widget_weekday', $wp_opening_hours->weekdays[ $key ] ) ?>
                    </th>
                    <td class="op-overview-times">
                    <? if (is_array( $periods ) and count( $periods )) : ?>
						<? foreach ($periods as $period) : ?>
                            <div class="op-overview-set <? echo ($instance['highlight'] == 'period' and $period->isRunning()) ? 'highlighted' : '' ?>">
                                <? echo apply_filters( 'op_overview_widget_time_string', timeString( array(
									'start'		=> $period->start_ts,
									'end'		=> $period->end_ts
								) ) );
								?>
                            </div>
                        <? endforeach ?>
                    <? else : ?>
                    	<div class="op-overview-set closed">
                        	<? echo apply_filters( 'op_overview_widget_closed', $instance['caption-closed'] ) ?>
                        </div>
                    <? endif ?>
                    </td>
                </tr>
            <?
				endif; 
				endforeach ;
			?>
            </table>
        <?
		endif;
		
		echo $after_widget;
	}
	
	function update ($new_instance, $old_instance) {
		return $new_instance;
	}
	
	function form ($instance) {
	?>
    	<p>
        	<label for="<? echo $this->get_field_id('title') ?>">
            	<? op_e('Title') ?>
            </label>
            <input type="text" value="<? echo $instance['title'] ?>" name="<? echo $this->get_field_name('title') ?>" id="<? echo $this->get_field_id('title') ?>" placeholder="<? echo $this->defaults['title'] ?>" class="widefat" />
        </p>
                
        <p>
        	<label for="<? echo $this->get_field_id('show-closed') ?>">
            	<input type="checkbox" <? echo ($instance['show-closed']) ? 'checked="checked"' : '' ?> name="<? echo $this->get_field_name('show-closed') ?>" id="<? echo $this->get_field_id('show-closed') ?>" />
                <? op_e('Also show closed days') ?>
            </label>
        </p>
        
    	<p id="op-overview-caption-closed">
        	<label for="<? echo $this->get_field_id('caption-closed') ?>">
            	<? op_e('Caption for "closed"-label') ?>
            </label>
            <input type="text" value="<? echo $instance['caption-closed'] ?>" name="<? echo $this->get_field_name('caption-closed') ?>" id="<? echo $this->get_field_id('caption-closed') ?>" placeholder="<? echo $this->defaults['caption-closed'] ?>" class="widefat" />
        </p>
        
        <p>
        	<label for="<? echo $this->get_field_id('highlight') ?>">
            	<? op_e('Highlight:') ?>
            </label>
            
            <select class="widefat" name="<? echo $this->get_field_name('highlight') ?>" id="<? echo $this->get_field_id('highlight') ?>">
            <?
				$highlight_opts		= array(
					'nothing'		=> op__('nothing'),
					'period'		=> op__('running period'),
					'day'			=> op__('current day')
				);
				
				foreach ($highlight_opts as $slug => $caption) :
				?>
                	<option value="<? echo $slug ?>" <? echo ($instance['highlight'] == $slug) ? 'selected="selected"' : '' ?>>
                    	<? echo $caption ?>
                    </option>
                <?
				endforeach;
			?>
            </select>
        </p>
    <?
	}
	
	function setup_defaults( $instance = array() ) {
		foreach ($this->defaults as $key => $caption) :
			if (empty($instance[ $key ]))	$instance[ $key ]	= $caption;
		endforeach;
		
		return $instance;
	}
	
}

class Opening_Hours_Status extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' 	=> 'widget_op_status', 
			'description' 	=> op__('This Widgets displays information in your sidebar, wether your venue is closed or open.')
		);
	
		$this->WP_Widget('widget_op_status', op__('Opening Hours Status'), $widget_ops);
		$this->alt_option_name = 'widget_op_status';
		
		$this->defaults = array(
			'caption-open'				=> apply_filters( 'op_status_widget_default_open', op__('We\'re currently open.') ),
			'caption-closed'			=> apply_filters( 'op_status_widget_default_closed', op__('We\'re currently closed.') ),
			'caption-closed-holiday'	=> apply_filters( 'op_status_widget_default_closed_holiday', op__('We\'re currently on holiday.') )
		);
	}
	
	function widget ($args, $instance) {
		extract ($args);
				
		$instance	= self::setup_defaults( $instance );
		
		echo $before_widget;
		
		if ($instance['title'])	:
			echo $before_title;
			echo apply_filters( 'op_status_widget_title', $instance['title'] );
			echo $after_title;
		endif;
		
		$is_open		= is_open( true ); 
		?>
        	<div class="op-status-label <? echo ($is_open[0]) ? 'open' : 'closed' ?> <? if (!$is_open[0] and $is_open[1] == 'holiday') echo 'closed-holiday' ?>">
            	<?
				
				if ($is_open[0]) :
					$message	= apply_filters( 'op_status_widget_open', $instance['caption-open'] );
				else :
					$message	= ($is_open[1] == 'holiday') 
						? apply_filters( 'op_status_widget_closed_holiday', $instance['caption-closed-holiday'] ) 
						: apply_filters( 'op_status_widget_closed', $instance['caption-closed'] );
				endif;
				
				echo apply_filters (
					'op_status_widget_output',
					$message
				)
				?>
            </div>
        <?
		echo $after_widget;
	}
	
	function update ($new_instance, $old_instance) {
		return $new_instance;
	}
	
	function form ($instance) {
	?>
    	<p>
        	<label for="<? echo $this->get_field_id('title') ?>">
            	<? op_e('Title') ?>
            </label>
            <input type="text" class="widefat" name="<? echo $this->get_field_name('title') ?>" id="<? echo $this->get_field_id('title') ?>" value="<? echo $instance['title'] ?>" />
        </p>
        
        <p>
        	<label for="<? echo $this->get_field_id('caption-open') ?>">
            	<? op_e('Custom open-message') ?>
            </label>
            <input type="text" class="widefat" name="<? echo $this->get_field_name('caption-open') ?>" id="<? echo $this->get_field_id('caption-open') ?>" value="<? echo $instance['caption-open'] ?>" placeholder="<? echo $this->defaults['caption-open'] ?>" />
        </p>
        
        <p>
        	<label for="<? echo $this->get_field_id('caption-closed') ?>">
            	<? op_e('Custom closed-message') ?>
            </label>
            <input type="text" class="widefat" name="<? echo $this->get_field_name('caption-closed') ?>" id="<? echo $this->get_field_id('caption-closed') ?>" value="<? echo $instance['caption-closed'] ?>" placeholder="<? echo $this->defaults['caption-closed'] ?>" />
        </p>
        
        <p>
        	<label for="<? echo $this->get_field_id('caption-closed-holiday') ?>">
            	<? op_e('Custom closed-holiday-message') ?>
            </label>
            <input type="text" class="widefat" name="<? echo $this->get_field_name('caption-closed-holiday') ?>" id="<? echo $this->get_field_id('caption-closed-holiday') ?>" value="<? echo $instance['caption-closed-holiday'] ?>" placeholder="<? echo $this->defaults['caption-closed-holiday'] ?>" />
        </p>

    <?
	}
	
	function setup_defaults( $instance = array() ) {
		foreach ($this->defaults as $key => $caption) :
			if (empty($instance[ $key ]))	$instance[ $key ]	= $caption;
		endforeach;
		
		return $instance;
	}


}
?>