<?
/**
 *	Template Tags
 */

// Returns bool wether vanue is open or not 
function is_open( $return_type = false ) {
	global $wp_opening_hours;
	
	if (!is_object($wp_opening_hours))	$wp_opening_hours	= new OpeningHours;
	
	
	$today		= strtolower( date('l', current_time('timestamp')) );
	
	// Holidays
	foreach ((array) $wp_opening_hours->holidays as $holiday) :
		if ($holiday->isRunning())	return ($return_type) ? array( false, 'holiday' ) : false;
	endforeach;
	
	// Opening Periods
	foreach ((array) $wp_opening_hours->$today as $period) :

		if ($period->isRunning())	return ($return_type) ? array( true, 'regular' ) : true;
	endforeach;
	
	return ($return_type) ? array( false, 'regular' ) : false;
}

// Returns the opposite of is_open()
function is_closed() {
	return !is_open();
}
?>