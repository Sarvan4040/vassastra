<?
/**
*	Register Styles
*/
function op_register_styles_backend() {
	wp_enqueue_style('opening-hours-backend', apply_filters( 'op_backend_stylesheet', op_baseurl().'/css/backend.css' ), false, false, 'all');
	wp_enqueue_style('jQuery-ui-style', 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css', false, false, 'all');
	
	wp_register_script('jQuery-ui', 'http://code.jquery.com/ui/1.10.3/jquery-ui.js', false, null, false);
	wp_enqueue_script('jQuery-ui');
}
add_action('admin_init', 'op_register_styles_backend');

function op_register_styles_frontend() {
	wp_register_style('opening-hours-frontend', apply_filters( 'op_frontend_stylesheet', op_baseurl().'/css/frontend.css' ), false, false, 'all');
	wp_enqueue_style( 'opening-hours-frontend' );
}
add_action('wp_enqueue_scripts', 'op_register_styles_frontend');

/**
 *	Create Opening Hours Instance
 */
$wp_opening_hours	= new OpeningHours;

/**
*	Register Backend Options Pages
*/
function op_register_options_pages() {
	// Top level menu item
	add_menu_page(
		apply_filters( 'op_menu_title_opening_hours', __('Opening Hours', op_textdomain()) ),
		apply_filters( 'op_menu_title_opening_hours', __('Opening Hours', op_textdomain()) ),
		apply_filters( 'op_min_user_capability', 'manage_options' ),
		'opening-hours',
		'op_setup_page'
	);
	
	// Holidays Page
	add_submenu_page(
		'opening-hours',
		apply_filters( 'op_menu_title_holidays', op__('Holidays') ),
		apply_filters( 'op_menu_title_holidays', op__('Holidays') ),
		apply_filters( 'op_min_user_capability', 'manage_options' ),
		'opening-hours-holidays',
		'op_holidays_page'
	);
	
	// Settings Page
	add_submenu_page (
		'opening-hours',
		apply_filters( 'op_menu_title_settings', op__('Settings') ),
		apply_filters( 'op_menu_title_settings', op__('Settings') ),
		apply_filters( 'op_min_user_capability', 'manage_options' ),
		'opening-hours-settings',
		'op_settings_page'
	);
}

function op_setup_page() {
	global $wp_opening_hours;
	// include template
	require_once	op_basepath() . '/templates/setup-page.php';
}

function op_holidays_page() {
	global $wp_opening_hours;
	// include template
	require_once	op_basepath() . '/templates/holidays-page.php';
}

function op_settings_page() {
	global $wp_opening_hours;
	// include template
	require_once	op_basepath() . '/templates/settings-page.php';
}

add_action ('admin_menu', 'op_register_options_pages');
?>