=== Opening Hours ===
Contributors: janizde
Tags: opening hours, venue, widget, shortcode, currently open, holidays
Requires at least: 3.0.1
Tested up to: 3.7.1
Stable tag: 1.1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Opening Hours Plugin lets you set up your venue’s opening hours in a simple GUI and displays them in two Widgets and a Shortcode

== Description ==

When having activated the plugin you can set up your venue’s opening hours in a couple of seconds on an extra options-page. You can add several periods for each day, e.g. if you’re having a lunch break.
Now that the Plugin knows when your venue is open and when it is closed you can chose any ways among the following to show them in your front end:

* The ‚Opening Hours Overview‘ Widget: This Widget lists up your Opening Hour in a table. The Special thing about it is, that it highlights the currently running opening period. You can also select either to display the days your venue is closed or not and add an individual caption for this case (see Screenshots). In the front end it perfectly integrates with most templates, as there is hardly any individual theming in the Widget itself.
* The ‚Opening Hours Status‘ Widget: This Widget checks wether your venue is currently open or not and displays a text-string that you can customize yourself. You can also easily style the Widget via your theme’s style.css
* The ‚Opening Hours Holidays‘ Widget: This Widget lists up all your Holidays that you’ve previously set up the settings.
* The is_open() or is_closed() Template Tags: These Template Tags are functions that return a bool wether your venue is open or not (true/false). Use this in your Theme.
* is-open Shortcode: This Shortcode basically does what the Opening Hours Status Widget does. But with the shortcode you can dynamically include the opening status in your posts and pages. You can also customize the text output via the attributes.

The Plugin has language support for English, German, Dutch, Czech and Polish (more to follow).

Features to come with the next mayor version:

1. Individual Opening Hours (e.g. Christmas Days)

== Installation ==

1. Download the .zip-archive
1. Unzip the archive
1. Upload the directory /opening-hours to your wp-content/plugins
1. In your Admin Panel go to Plugins and active the Opening Hours Plugin
1. Now you can edit your Opening Hours in the Settings-Section
1. Place the Widgets in your Sidebars or use the Shortcode in your posts and Pages

For more detailed information read through Jeff Matson’s (@JeffMatson) Installation and Configuration Blog-Post:
http://www.inmotionhosting.com/support/website/wordpress-plugins/opening-hours-plugin-for-wordpress

== Frequently Asked Questions ==



== Screenshots ==

1. The default WordPress 3.8 Theme with a couple of the Opening Hours Widgets and the Shortcode used in the post content.
2. The form to set up your regular Opening Hours. 
3. The form to set up your holidays with a datepicker open.
4. The form to set up your Special Openings.
5. The general settings page. Choose among different date and time formats or define your own.
6. Three of the four Opening Hours Widgets in the Widgets-Menu.
7. The new jQuery UI timepicker makes setting up your hours easier than before.



== Changelog ==

= 1.0 =
initial version

= 1.0.1 =
fixed a bug that displayed saturday instead of friday
ATTENTION: REINSTALL NECCESSARY!

= 1.1 =
Read this article:
http://www.jannikportz.de/2013/11/03/opening-hours-update-version-1-2/

= 1.1.1 =
Read this article:
http://www.jannikportz.de/2013/12/04/opening-hours-update-1-1-1/

== Translations ==
To provide the Plugin in the greatest possible number of different languages I need you to help me translating the strings.
If you want to take part in the translation team at http://translate.jannikportz.de and register for an account. If you need a new language set, just let me now.

Thanks

Current Translation Team:
@janizde (German)
@jholasek (Czech)
@Patricksit (Dutch)
@servantez (Polish)

I do not update this list frequently so it might be a few more at this time ;)