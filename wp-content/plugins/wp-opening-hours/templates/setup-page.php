<?
/**
 *	Backend Options Page: Setup
 */

$weekdays		= array(
	'monday'		=> op__('Måndag'),
	'tuesday'		=> op__('Tuesday'),
	'wednesday'		=> op__('Wednesday'),
	'thursday'		=> op__('Thursday'),
	'friday'		=> op__('Friday'),
	'saturday'		=> op__('Saturday'),
	'sunday'		=> op__('Sunday')
);

// Process Data
if ($_POST['action']	== 'save') :
	if (!is_object($wp_opening_hours))	$wp_opening_hours	= new OpeningHours;
	
	foreach ($weekdays as $key => $caption) :
		$periods	= array();
		$i			= 0;
		
		foreach ($_POST['op-' . $key . '-start-hour'] as $start_hr) :
			$data		= array(
				'day'		=> $key,
				'times'		=> array(
					$_POST['op-' . $key . '-start-hour'][$i],
					$_POST['op-' . $key . '-start-minute'][$i],
					$_POST['op-' . $key . '-end-hour'][$i],
					$_POST['op-' . $key . '-end-minute'][$i]
			));
						
			$periods[]	= new OpeningPeriod( $data );
			$i++;
		endforeach;
		
		$wp_opening_hours->$key		= $periods;
	endforeach;
		
	$wp_opening_hours->save();	// save data in wp_option
	
endif;

$wp_opening_hours->addDummySets();

?>
<div class="wrap">
	
    <? screen_icon('options-general') ?>

    <h2>
        <? op_e('Opening Hours') ?>
    </h2>
    
    <p>
        <? op_e('Setup your Opening Hours in this form. You can add several periods per day, e.g. if you have a lunch break.') ?>
    </p>
    
    <!-- Opening Hours Form -->
    <form method="post">
    	<table class="form-table" id="op-options-times">
        <?			
			$hours			= range( 0, 24 );
			$minutes		= range( 0, 55, 5 );
			
			foreach ( $weekdays as $key => $caption ) :
			?>
            	<tr id="op-row-<? echo $key ?>">
                	<th class="op-day-heading">
                    	<label for="toggle-<? echo $key ?>">
								<? echo $caption ?>
                        </label>
                    </th>
                    <td class="op-day-options">
                        <div class="op-times-container" id="op-times-container-<? echo $key ?>">
                        	<? foreach ($wp_opening_hours->$key as $i => $period) : ?>
                            <div class="op-time-group">
                            	<div class="op-time-set">
                                    <select name="op-<? echo $key ?>-start-hour[]" size="1" class="op-label first op-select op-select-<? echo $key ?>">
                                        <? 
											foreach ($hours as $hour) :
											$selected	= ($hour == $period->val('start', 'hour')) ? 'selected="selected"' : '';
										?>
                                        <option value="<? echo $hour ?>" <? echo $selected ?>><? echo twoDigits( $hour ) ?></option>
                                        <? endforeach ?>
                                    </select><span class="op-label mid">:</span><select name="op-<? echo $key ?>-start-minute[]" size="1" class="op-label last op-select op-select-<? echo $key ?>">
                                        <? 
											foreach ($minutes as $minute) : 
                                        	$selected	= ($minute == $period->val('start', 'minute')) ? 'selected="selected"' : '';
										?>
                                        <option value="<? echo $minute ?>" <? echo $selected ?>><? echo twoDigits( $minute ) ?></option>
                                        <? endforeach ?>
                                    </select>
                                </div>
                                &nbsp;–&nbsp;
                            	<div class="op-time-set">
                                    <select name="op-<? echo $key ?>-end-hour[]" size="1" class="op-label first">
                                        <? 
											foreach ($hours as $hour) :
										   	$selected	= ($hour == $period->val('end', 'hour')) ? 'selected="selected"' : '';
										?>
                                        <option value="<? echo $hour ?>" <? echo $selected ?>><? echo twoDigits( $hour ) ?></option>
                                        <? endforeach ?>
                                    </select><span class="op-label mid">:</span><select name="op-<? echo $key ?>-end-minute[]" size="1" class="op-label last">
                                        <? 
											foreach ($minutes as $minute) : 
                                        	$selected	= ($minute == $period->val('end', 'minute')) ? 'selected="selected"' : '';
										?>
                                        <option value="<? echo $minute ?>" <? echo $selected ?>><? echo twoDigits( $minute ) ?></option>
                                        <? endforeach ?>
                                    </select>
                                </div>
                                <? if ($i == 0) : ?>
                                    <a class="op-label green op-add-period" data-key="<? echo $key ?>">+ <? op_e('Add Period') ?></a>
                                <? else : ?>
                                	<a class="op-label red op-remove-period" data-key="<? echo $key ?>"><? op_e('Remove') ?></a>
                                <? endif ?>
                            </div>
                            <? endforeach ?>
                        </div>
                    </td>
                </tr>
            <?
			endforeach;
		?>
        </table>
        
        <input type="hidden" name="action" value="save" />
        <?
			submit_button()
		?>
    </form>
</div>

<script type="text/javascript">
	
	function initActions() {
		jQuery('.op-add-period, .op-remove-period').unbind();
		
		jQuery('.op-add-period').click(function(e) {
			e.preventDefault();
			addPeriod (jQuery(this));
		});
		
		jQuery('.op-remove-period').click(function(e) {
			e.preventDefault();
			removePeriod (jQuery(this));
		});
	}
		
	initActions();

	function addPeriod (element) {
		key			= element.attr('data-key');
		container	= jQuery('#op-times-container-'+key);
		newGroup	= container.children('.op-time-group').first().clone();
		option		= newGroup.find('.op-add-period');
		console.log(option);
		option.removeClass('green').removeClass('op-add-period').addClass('red').addClass('op-remove-period').html('<? op_e('Remove') ?>');
		newGroup.appendTo( container );
		initActions();
	}
	
	function removePeriod (element) {
		element.parent('.op-time-group').remove();
		initActions();
	}
</script>