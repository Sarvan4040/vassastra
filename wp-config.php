<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vassastra_se');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eSrrZV$)E,it}|V(XWSx]c55#am}B|TJsrgrTZ9%x:2IJvy5=WqE!^Btq3L.}O3z');
define('SECURE_AUTH_KEY',  'M`T%}mEPC2[Ke{J}I$Y<T,:pt>Mtj#cHG!.Z5 [&}{4Y(yudklTG}I eXX26-Qu+');
define('LOGGED_IN_KEY',    'pVQx8X,}(Boe%F@}G:aX/`@tr3NXZ1Qqnt4j`G. @R)-[2b}tf4kUKB0_u?fuF2X');
define('NONCE_KEY',        'Jy^Tb48_`G*/PpR0:^Cwv7_{s~77@Dx~>2.z}5G0mBpDQPe_KVx}VLiJn$LdQ=1s');
define('AUTH_SALT',        'U^Q~ZRlV|c5hryEv&4[wz/Zky G4Tl=>`,B9hri`h0/2]?YvO$QYoA3=h1ldVJ}+');
define('SECURE_AUTH_SALT', 'P8Uglem9XF3CKp#*,QAG<|W,a,LlltK7VFC }OVUf>58OUd[Q]shKLzSHk9^SXim');
define('LOGGED_IN_SALT',   'n?Rqhn,aB0<Cb.Y=s|J2$6L_EBIbwpnvUVw/+6U~gPDbY-D27E~[uyp}o6:x~b=(');
define('NONCE_SALT',       'Vp4~+s?BFk}nKVr?!f%~0sfhb0C7`s7*,5~W|ZLedA/RscZi/7Pd`cUf5*SZP!eu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
